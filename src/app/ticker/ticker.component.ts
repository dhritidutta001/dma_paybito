import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";
import * as Stomp from 'stompjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import * as SockJS from 'sockjs-client';
import { CexioService } from '../services/cexio.service'
import { Subscription } from 'rxjs';
import { Ticker } from './../core-data-veriable';
import { CoreDataService } from '../core-data.service';
import * as io from 'socket.io-client';
import { PaybitoService } from '../../app/services/paybito.service'
import {
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import { BinanceService } from '../services/binance.service';
import { JsonPipe } from '@angular/common';
//const socket = io('http://50.18.49.160:3000');

//declare const DisplayMessage: any;
interface Country {
  name: string;
  flag: string;
  area: number;
  population: number;


}
@Component({
  selector: 'app-ticker',
  templateUrl: './ticker.component.html',
  styleUrls: ['./ticker.component.css']
})
export class TickerComponent implements OnInit {
  tickerdata: any;
  shortcutdata = [];
  // tradedata: any = "";
  // offerdata: any = "";
  serarchpair = [];
  private tadeapi: Subscription;
  private offerapi: Subscription;
  ticker: Ticker;
  nodataMsg: any = "";
  //ClientoffId: any;
  // Offstatus: any;
  tickerasset: any;
  accountInfo: any;
  userId: any;
  // accessToken: any;
  // myBalanceList: any;
  alltickeralue = [];
  // shorcutdatafinal: any;
  allOrders: any;
  alltrades = [];
  alloffer = [];
  acbalance = [];
  maker: any;
  taker: any;
  searchsymbol: any;
  asset: any;
  sendaddress: any;
  receiveaddress: any;
  amount: any;
  constructor(public binanceService: BinanceService, private _pService: PaybitoService, private modalService: NgbModal, private http: HttpClient, private data: CoreDataService, private _CexioService: CexioService) { }
  //public active = 1;
  //countries = COUNTRIES;
  ngOnInit(): void {
    //  DisplayMessage();


    // this.allassetsticker();

    this.tickerdata = localStorage.getItem('ticker');
    var assetpair = this.data.symbol;// this.data.SelectedBuyasset + '/' + this.data.SelectedSellasset;
    this.binanceService.gettickerdata();
    this.binanceService.getOnertickerdata();
    $('.table-msr-search').display = 'none';
    $('.table-msr').display = 'block';
    //  this.alltickeralue=this.binanceService.alltickers;
    // this.getdata();
    // this._OrderbookComponent.Getdata();
    //this.alltickeralue=this._OrderbookComponent.alltickers;


    // socket.on('connect', function () {
    //   socket.emit('join', assetpair);
    // });
    //  setTimeout(() => {
    //   this._OrderbookComponent.Getticker();
    //   this.shorcutdatafinal =this._OrderbookComponent.alltickers;
    //   console.log('oooooooooooo}}}}}}}}}}}',this.shorcutdatafinal);
    // }, 5000);




  }
  Getalltickertable() {
    document.getElementById('table-msr').style.display = 'block';
    document.getElementById('oneticker').style.display = 'none';
  
  }
  myFunction() {
    //  alert('rrrrrrrr');
    //this.binanceService.discunnectSocket();
   
  //  this.binanceService.discunnectSocket();
    document.getElementById('table-msr').style.display = 'none';
    document.getElementById('oneticker').style.display = 'block';

    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    //alert(filter);

    this.binanceService.SendSymboltoServer(filter);
    this.binanceService.getOnertickerdata();
    // if(input==null){
    //   $(".table-msr").removeClass("search-msr");
    // }
    // filter = input.value.toUpperCase();
    // table = document.getElementById("table-msr");
    // tr = table.getElementsByTagName("tr");
    // for (i = 0; i < tr.length; i++) {
    //   td = tr[i].getElementsByTagName("td")[0];
    //   if (td) {
    //     txtValue = td.textContent || td.innerText;
    //     if (txtValue.toUpperCase().indexOf(filter) > -1) {
    //       tr[i].style.display = "";
    //     } else {
    //       tr[i].style.display = "none";
    //     }
    //   }
    // }

  }
  searchasset() {

    // alert(this.searchsymbol);
    // $('.table-msr-search').display='block';
    // $('.table-msr').display='none';
    document.getElementById('table-msr-search').style.display = 'block';
    document.getElementById('table-msr').style.display = 'none';
    var symbol = this.searchsymbol.toUpperCase();
    if (symbol != "") {
      for (var k = 0; k < this.binanceService.alltickers.length; k++)

        var testarry = JSON.parse(JSON.stringify(this.binanceService.alltickers));
      console.log('-------------------ddddddddddddddddd', testarry[0].symbol);
      if (testarry[k].symbol == symbol) {
        alert('aaa');
        //this.serarchpair.push({'symbol':this.binanceService.alltickers[z].symbol,'priceChange':this.binanceService.alltickers[z].priceChange,'bestAskPrice':this.binanceService.alltickers[z].bestAskPrice,'bestAskQuantity':this.binanceService.alltickers[z].bestAskPrice,'bestBid':this.binanceService.alltickers[z].bestAskPrice,'bestBidQuantity':this.binanceService.alltickers[z].bestBidQuantity,'closeQuantity':this.binanceService.alltickers[z].closeQuantity,'currentClose':this.binanceService.alltickers[z].currentClose,'open':this.binanceService.alltickers[z].open,'low':this.binanceService.alltickers[z].low,'high':this.binanceService.alltickers[z].bestAskPrice,'previousClose':this.binanceService.alltickers[z].bestAskPrice,'quoteAssetVolume':this.binanceService.alltickers[z].quoteAssetVolume})
      }
    }

  }
 

  buttonclick() {
    //  $('.wl').toggleClass('plus');
    $("#panelticker").slideToggle();
    document.getElementById("minimizticker").style.display = 'none';
    document.getElementById("restoreticker1").style.display = 'block';
    //   $(".wl").animate({

    //     height: '100%',

    //  });
  }
  restorewindow1() {
    $("#panelticker").slideToggle();
    document.getElementById("restoreticker1").style.display = 'none';
    document.getElementById("minimizticker").style.display = 'block';

  }
  maximizewindow() {
    document.getElementById("ordershortcut").style.display = 'none';
    document.getElementById("tvchart").style.display = 'none';
    document.getElementById("demosor").style.display = 'none';
    document.getElementById("orderbook-box").style.display = 'none';
    document.getElementById("tickerview").style.display = 'none';
    document.getElementById("tickermax").style.display = 'block';
    //  document.getElementById("restoreticker").style.display='block';
    //  document.getElementById("maximizeticker").style.display='none'; 
    //   $(".wl").animate({
    //     left: '5px',
    //     height: '100%',
    //     width: '125%',
    //  });
    $('.table-msr').addClass("new-ht");
  }
  closemaxTickerwindow() {
    $('.table-msr').removeClass("new-ht");
    document.getElementById("ordershortcut").style.display = 'block';
    document.getElementById("tvchart").style.display = 'block';
    document.getElementById("demosor").style.display = 'block';
    document.getElementById("orderbook-box").style.display = 'block';
    document.getElementById("tickerview").style.display = 'block';
    document.getElementById("tickermax").style.display = 'none';
  }
  restorewindow() {
    //

    //document.getElementById("tickerview").style.display = 'none';
    document.getElementById("restoreticker").style.display = 'none';
    document.getElementById("maximizeticker").style.display = 'block';
    $('#table-msr').removeClass("new-ht");

    $(".wl").animate({

      height: '60%',
      width: '64%'
    }

    );
    document.getElementById("minimizticker").style.display = 'block';
    document.getElementById("ordershortcut").style.display = 'block';
    document.getElementById("tvchart").style.display = 'block';
    document.getElementById("demosor").style.display = 'block';
    document.getElementById("marketwatch").style.display = 'block';
    // document.getElementById("tickerview").style.display = 'block';
  }

  //   float: left;
  //   margin-left: 2px;
  //   padding: 2px;
  //   height:60%;
  //   width: 64%;
  //  margin-bottom: 2px;
  // buttonclick1(){
  //   $('#min').removeClass("btn-plus");
  //   $('#min').addClass("min");

  //   // $('#min').toggleClass('min');
  //   // $(".body-dw").slideToggle();
  //   // document.getElementById("minimize").style.display='block';
  //  document.getElementById("maximize").style.display='none';
  // }
  // getdata() {
  //   socket.on('data1', (res) => {
  //     this.tickerdata = res.msg;
  //     //  var tdata=JSON.parse(data);
  //     // this.tickerdata=tdata.data
  //     // console.log('ddddddddddddddddddddddddd',this.tickerdata);
  //     localStorage.setItem('ticker', this.tickerdata);
  //     // this.data.Lasttraderprice = this.tickerdata.last;;

  //   })

  //   socket.on('data2', (res) => {
  //     var data = res.msg;

  //     var symbolone = data.symbol1;
  //     var symboltwo = data.symbol2;
  //     // if(this.shortcutdata.length<=2){
  //     this.shortcutdata.push({
  //       open24: data.open24,
  //       price: data.price,
  //       symbol1: data.symbol1,
  //       symbol2: data.symbol2,
  //       volume: data.volume

  //     })
  //     var newassetpair = data.symbol1 + data.symbol2;
  //     var selectedasset = this.data.SelectedBuyasset + this.data.SelectedSellasset;
  //     if (newassetpair == selectedasset) {

  //       this.data.Lasttraderprice = data.price;
  //       // alert( this.data.Lasttraderprice);
  //     }
  //     // }
  //     if (this.shortcutdata.length > 2) {
  //       for (var i = 0; i < this.shortcutdata.length; i++) {

  //         if (symbolone == this.shortcutdata[i].symbol1 && symboltwo == this.shortcutdata[i].symbol2) {
  //           // this.shortcutdata.splice(i,5);

  //           this.shortcutdata[i].price = data.price;


  //         }

  //         var selectedasset = this.data.SelectedBuyasset + this.data.SelectedSellasset;

  //         var assetpair = this.shortcutdata[i].symbol1 + this.shortcutdata[i].symbol2;
  //         if (selectedasset == assetpair) {

  //           this.data.Lasttraderprice = this.shortcutdata[i].price;
  //           // alert( this.data.Lasttraderprice);
  //         }
  //       }

  //     }


  //   })

  // }
  closeTickerwindow() {
    document.getElementById("tickerview").style.display = 'none';
  }
  // allassetsticker() {
  //   this.data.loader = true;
  //   var url = "https://stream.paybito.com/BrokerStreamApi/rest/getAllTickerList";
  //   if (this.data.source != undefined) {
  //     this.data.source.close();
  //   }
  //   if (this._pService.sourcebalance != undefined) {
  //     this._pService.sourcebalance.close();
  //   }
  //   this.data.source = new EventSource(url);

  //   var result: any = new Object();
  //   this.data.source.onmessage = (event: MessageEvent) => {
  //     result = JSON.parse(event.data);
  //     this.tickerasset = result;
  //     this.data.loader = false;
  //     if (this.tickerasset == null || this.tickerasset == null) {
  //       this.nodataMsg = "No Data";

  //     }
  //     //console.log('result--------------', result);
  //   }
  //   // if (this.token.length >= "null" || this.token.length >= 0) {}
  // }
  openTickerwindow() {
    document.getElementById("tickerview").style.display = 'block';
  }
  closewindow() {
    //debugger;
    //  DisplayMessage();
    // $('#tickerview').find('.ticker-tab').close();
    // $('#tickerview').find('.ticker-tab').animate({'left':0,'bottom':0});
  }
  Getallarders() {
    this.alltrades = [];
    this.alloffer = [];
    // var assetpair = this.data.SelectedBuyasset + this.data.SelectedSellasset;

    var assetpair = this.data.symbol;
    //localStorage.getItem('symbol');

    this.binanceService.allOrders(assetpair)
      .subscribe(data => {
        //   console.log('orders---------', data);
        this.allOrders = data;
        for (var i = 0; i < this.allOrders.length; i++) {
          if (this.allOrders[i].status == 'FILLED') {
            this.alltrades.push(this.allOrders[i]);
            JSON.stringify(this.alltrades);
          }
          else if (this.allOrders[i].status == 'CANCELED') {
            this.alloffer.push(this.allOrders[i]);
            JSON.stringify(this.alloffer);
          }

        }
        //  console.log('or---------', this.alltrades);
        //  console.log('offers---------', this.alloffer);
      })


  }
  GetAccountInfo() {
    this.binanceService.AccountInfo()
      .subscribe(data => {
        if(data){
        this.acbalance = JSON.parse(JSON.stringify(data.balances));
        //  this.acbalance = this.accountInfo.balances;
        this.maker = JSON.stringify(data.makerCommission);
        // this.maker=JSON.stringify(this.accountInfo.makerCommission);
        this.taker = JSON.stringify(data.takerCommission);
        // console.log(this.accountInfo);//'Invalid API-key, IP, or permissions for action."
        console.log('balance---------------------', this.acbalance[0].asset);
        if (this.acbalance == null) {
          this.nodataMsg = "Login to view data";
        }
      }
      else{
        this.data.alert('Invalid API-key, IP, or permissions for action.','danger');
      }
      })
  }
  Sendcurrency(sendcurrency, currency) {
    // alert(currency);
    this.asset = currency;
    this.modalService.open(sendcurrency, {
      centered: true

    });
    this.Getdepositaddress();

  }
  copy(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }
  Getcurrency(sendcurrency, currency) {
    this.asset = currency;
    this.modalService.open(sendcurrency, {
      centered: true

    });
  }
  Getdepositaddress() {

    this.binanceService.getdipositaddress(this.asset)
      .subscribe(data => {
        var successmessaage = data.success;
        var errormsg = data.msg;
        console.log('------------------', successmessaage);
        if (successmessaage == true) {
          this.sendaddress = data.address;
          console.log('------------------', this.sendaddress);
          this.data.alert(errormsg, 'success');
        }
        else {
          this.data.alert(errormsg, 'danger');
        }

      })

  }
  withdraw() {
    var address = this.receiveaddress;
    var amount = this.amount;
    this.binanceService.submitwithdowrequest(this.asset, address, amount)
      .subscribe(data => {
        var successmessaage = data.success;
        console.log('------------------', successmessaage);
        if (successmessaage == true) {
          var message = data.msg;

          console.log('------------------', this.sendaddress);
          this.data.alert(message, 'success');
        }
        else {
          var errormsg = data.msg;

          this.data.alert(errormsg, 'danger');
        }


      })
  }
  // traderdata() {
  //   //alert('rrrrrrrrrrr');
  //   if (this._pService.sourcebalance != undefined) {
  //     this._pService.sourcebalance.close();
  //   }
  //   this.data.alert("Loading...", "dark");
  //   this.data.loader = true;
  //   var tradedata = {};
  //   var userId = localStorage.getItem('userId');
  //   tradedata['userId'] = userId;
  //   tradedata['symbol'] = this.data.SelectedBuyasset + "/" + this.data.SelectedSellasset;
  //   tradedata['pageNo'] = 1;
  //   tradedata['noOfItemsPerPage'] = 20;;
  //   if (userId != null && userId != undefined) {
  //     this._CexioService.Getalltrade(tradedata)
  //       .subscribe(data => {
  //         console.log('TTTTTTTTTTT', data.orderListResult);
  //         this.tradedata = data.orderListResult;
  //         this.data.loader = false;
  //         if (this.tradedata == null || this.tradedata == null) {
  //           this.nodataMsg = "No Data"

  //         }
  //         // clientOrderId: "1597143342574-1"
  //         // commisionType: "3"
  //         // commisison: 0.0432306
  //         // cumulativeQty: 21.6153
  //         // execId: "1596436900721_103_55"
  //         // execType: "F"
  //         // leavesQty: 0.3847
  //         // msgType: "8"
  //         // orderDesc: "{"cumulativeAmountCcy1":"0.00180000","cumulativeAmountCcy2":"21.61530000"}"
  //         // orderId: "25179"
  //         // orderMasterId: "346dbec0-dbc1-11ea-8f3b-bf34d0d0792f"
  //         // orderQty: 22
  //         // orderStatus: "2"
  //         // orderType: "1"
  //         // origOrderId: null
  //         // price: 0
  //         // registID: null
  //         // side: "1"
  //         // symbol: "BTC/USD"
  //         // timeInForce: null
  //         // trnsTime: "20200811-10:55:44.193"
  //         // userId: 0

  //       })
  //   }
  //   else {
  //     this.nodataMsg = "No Data"
  //   }
  // }
  // getoffer() {
  //   if (this._pService.sourcebalance != undefined) {
  //     this._pService.sourcebalance.close();
  //   }
  //   this.data.alert("Loading...", "dark");
  //   this.data.loader = true;
  //   var offerdata = {};
  //   var userId = localStorage.getItem('userId');

  //   offerdata['userId'] = userId;
  //   offerdata['symbol'] = this.data.SelectedBuyasset + "/" + this.data.SelectedSellasset;
  //   offerdata['pageNo'] = 1;
  //   offerdata['noOfItemsPerPage'] = 20;;
  //   if (userId != null && userId != undefined) {
  //     this._CexioService.Getofferdata(offerdata)
  //       .subscribe(data => {
  //         if (data.error.error_data == 1) {

  //         }
  //         else {
  //           this.offerdata = data.orderListResult;
  //           this.data.loader = false;
  //           if (this.offerdata == null || this.offerdata == null) {
  //             this.nodataMsg = "No Data";

  //           }
  //           // console.log('?????????????????????', this.offerdata);
  //           //           averagePx: 0
  //           // clientOrderId: "1597217495114-1"
  //           // commisionType: "3"
  //           // commisison: 0
  //           // cumulativeQty: 0
  //           // execId: "1596436870611_100_61"
  //           // execType: "0"
  //           // leavesQty: 2
  //           // msgType: "8"
  //           // orderDesc: "{"cumulativeAmountCcy1":"0.00000000","cumulativeAmountCcy2":"0.00000000"}"
  //           // orderId: "25205"
  //           // orderMasterId: "da026f00-dc6d-11ea-982e-6bc8709f9ca7"
  //           // orderQty: 2
  //           // orderStatus: "0"
  //           // orderType: "2"
  //           // origOrderId: null
  //           // price: 11400
  //           // registID: null
  //           // side: "2"
  //           // symbol: "BTC/USD"
  //           // timeInForce: "1"
  //           // trnsTime: "20200812-07:31:35.376"
  //           // userId: 0
  //         }



  //       })
  //   }
  //   else {

  //     this.nodataMsg = "No Data";


  //   }
  // }
  // Getofferstatus(clientID, offer) {
  //   this.ClientoffId = clientID;
  //   // Offstatus
  //   if (this.ClientoffId != "") {
  //     this.modalService.open(offer, {
  //       centered: true
  //     });
  //     var Offerstatusobj = {};
  //     var userId = localStorage.getItem('userId');
  //     Offerstatusobj['userId'] = userId;
  //     Offerstatusobj['clientOrderId'] = this.ClientoffId;
  //     this._CexioService.GetOfferstatus(Offerstatusobj)
  //       .subscribe(data => {
  //         var offerdata = data.orderDetails;
  //         this.Offstatus = offerdata['orderStatus'];
  //         // alert(this.Offstatus);
  //         // console.log('OOOOOOOOOOO',data.orderDetails);
  //       })
  //   }
  // }
  // genarateauthtoken() {

  //   this.userId = '3050';
  //   let body = new URLSearchParams();
  //   body.set('username', this.userId);
  //   var password = 'Rpn@Mou8695';
  //   body.set('password', password);
  //   body.set('grant_type', 'password');

  //   this._pService.Getaccesstoken(body)
  //     .subscribe(dataAuth => {
  //       console.log('dataAuth', dataAuth);
  //       this.accessToken = dataAuth.access_token;
  //       localStorage.setItem('access_token', this.accessToken);
  //     })
  // }
  // userbalance() {

  //   this.genarateauthtoken();
  //   //this.data.alert("Loading...", "dark");
  //   this.data.loader = true;
  //   var userTransObj = {};
  //   var userId = localStorage.getItem('userId');
  //   //var jsonString = JSON.stringify(userTransObj);
  //   // this._pService.GetUserbalance(userTransObj)
  //   // .subscribe(data=>{
  //   //   var result =data;

  //   //    if (result.error.error_data != '0') {
  //   //      this.data.alert('Cannot fetch user balance', 'danger');
  //   //    } else {
  //   //      this.balencelist = result.userBalanceList;
  //   var url ="https://stream.paybito.com/BrokerStreamApi/rest/getBrokerCustomerWallet/" + '3050' + "";
  //   if (this._pService.sourcebalance != undefined) {
  //     this._pService.sourcebalance.close();
  //   }
  //   this._pService.sourcebalance = new EventSource(url);
  //   var result: any = new Object();
  //   this._pService.sourcebalance.onmessage = (event: MessageEvent) => {
  //    // result = event.data;
  //   var  result = JSON.parse(event.data);
  //     this.myBalanceList = result;
  //    console.log('LLLLLLLLLLLLLLLLLLLL',this.myBalanceList);
  //    //alert(this.myBalanceList);
  //    if( this.myBalanceList !=""|| this.myBalanceList!=null){
  //    // this.data.loader = false;
  //    }

  //   };
  //   // })
  // }
  // ngOnDestroy() {
  //   if (this.data.source != undefined) {
  //     this.data.source.close();
  //   }
  //   if (this._pService.sourcebalance != undefined) {
  //     this._pService.sourcebalance.close();
  //   }
  // }

}

