import { Component, OnInit } from '@angular/core';
import { Directive, EventEmitter, Input, Output, QueryList, ViewChildren } from '@angular/core';
import * as $ from "jquery";
import { CoreDataService } from '../core-data.service';
import { TickerComponent } from '../ticker/ticker.component';
import { BinanceService } from '../services/binance.service'

// @Directive({
//   selector: 'th[sortable]',
//   host: {
//     '[class.asc]': 'direction === "asc"',
//     '[class.desc]': 'direction === "desc"',
//     '(click)': 'rotate()'
//   }
// })
// export class NgbdSortableHeader {

//   @Input() sortable: SortColumn = '';
//   @Input() direction: SortDirection = '';
//   @Output() sort = new EventEmitter<SortEvent>();

//   rotate() {
//     this.direction = rotate[this.direction];
//     this.sort.emit({column: this.sortable, direction: this.direction});
//   }
// }



@Component({
  selector: 'app-demosor',
  templateUrl: './demosor.component.html',
  styleUrls: ['./demosor.component.css']
})
export class DemosorComponent implements OnInit {
  tickerasset: any;
  shorcutdatafinal: any;
  assets: any;
  symbol: any = 'XRPUSDT';
  Binanceticker = [];
  symbolarray = [];
  constructor(private data: CoreDataService, public tickerdata: TickerComponent, private binanceService: BinanceService) { }
  // countries = COUNTRIES;
  // nodataMsg:any;
  // @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  // onSort({column, direction}: SortEvent) {

  //   // resetting other headers
  //   this.headers.forEach(header => {
  //     if (header.sortable !== column) {
  //       header.direction = '';
  //     }
  //   });

  //   // sorting countries
  //   if (direction === '' || column === '') {
  //     this.countries = COUNTRIES;
  //   } else {
  //     this.countries = [...COUNTRIES].sort((a, b) => {
  //       const res = compare(`${a[column]}`, `${b[column]}`);
  //       return direction === 'asc' ? res : -res;
  //     });
  //   }
  // }

  ngOnInit(): void {

    this.Getbinanceasset();


  }
  Getbinanceasset() {
    this.binanceService.Getallassets()
      .subscribe(data => {
        this.assets = JSON.parse(JSON.stringify(data.symbols))
        //  console.log('assets--------------', this.assets);

      })
  }
  getcomparativevalue() {
    debugger;
    $(".table-msr").addClass("compare-msr");
    this.binanceService.gettickerdata()
    console.log('-------------deosor', this.binanceService.alltickers);
    var binanceticker = [];
    binanceticker = this.binanceService.alltickers
    var paybitoticker = [];

    if (binanceticker != null) {
      for (var a = 0; a < binanceticker.length; a++) {
        if (binanceticker[a].symbol == this.symbol) {
           const alligator = this.symbolarray;

           var found = alligator.includes(binanceticker[a].symbol); // returns true

           //console.log(found);
           if(found==false){
            this.Binanceticker.push(binanceticker[a]);
           }
          
         // for (var w = 0; w < this.Binanceticker.length; w++) {

            this.symbolarray.push(binanceticker[a].symbol);
         // }
          // if (this.Binanceticker.length == 0) {
          //   this.Binanceticker.push(binanceticker[a]);
          // }
          //   for (var w = 0; w < this.Binanceticker.length; w++) {
          //    var found = this.Binanceticker.find(element => element == binanceticker[a].symbol);
          //   //  alert(found);
          // //   var found = this.Binanceticker.find(function (element) { 
          // //     return element == binanceticker[a].symbol; 
          // // }); 

          // // Printing desired values. 
          // document.write(found); 

          //     if (found == undefined) {
          //       this.Binanceticker.push(binanceticker[a]);
          //     }
          //     // if(binanceticker[a].symbol!=this.Binanceticker[w].symbol){
          //     //   this.Binanceticker.push(binanceticker[a]);
          //     // }
          //     // const found = this.Binanceticker.find(element => element > 10);

          //     // else if(this.Binanceticker.length<2){
          //     //   this.Binanceticker.splice(w);
          //     // }
          //   }
        }
      }
      console.log('....>>>>>>>>>>>', this.Binanceticker);
    }


  }
  Deletcomparativevalue() {
    for (var x = 0; x < this.Binanceticker.length; x++) {
      if (this.Binanceticker[x].symbol == this.symbol) {
        this.Binanceticker.splice(x);
      }
    }
  }
  getAsset(event) {
    debugger;
    var assetpair = event;
    this.symbol = assetpair;
    //localStorage.setItem("symbol",this.symbol);


    // if (this.exchange == "cex.io" || this.exchange == undefined) {

    // }
    // else if (this.exchange = 'Itbit') {

    // }

    // console.log('////////////////dddddd',this.cryptoasset);
  }
  buttonclick() {
    // $('.minsor').toggleClass('btn-plus');
    $('#panelsor').slideToggle();
    // $('.demosor-body').animate({left: '250px'});
    $(".demosor-tab").animate({
      // left: '60%',
      height: '10%',
      width: '30%',
    });
    // $('.charts').toggleClass('but-plus'); 
    //  $('.demosor-tab').addClass("minusbtn");
    document.getElementById("restoresor").style.display = 'block';
    document.getElementById("minimizesor").style.display = 'none';
    document.getElementById("closesor").style.display = 'none';
    document.getElementById("closesorMax").style.display = 'none';
    document.getElementById("maximizesor").style.display = 'none';
  }
  restorewindow() {
    $('#panelsor').slideToggle();
    //  $('.demosor').removeClass("minusbtn");
    $(".demosor-tab").animate({

      height: '50%',
      width: '100%',
    });
    document.getElementById("restoresor").style.display = 'none';
    document.getElementById("minimizesor").style.display = 'block';
    document.getElementById("closesor").style.display = 'block';
    document.getElementById("maximizesor").style.display = 'block';
  }
  maximizewindow() {
    document.getElementById("ordershortcut").style.display = 'none';
    document.getElementById("tvchart").style.display = 'none';
    document.getElementById("tickerview").style.display = 'none';
    document.getElementById("dataTab").style.display = 'none';
    document.getElementById("closesor").style.display = 'none';
    document.getElementById("maximizesor").style.display = 'none';
    document.getElementById("closesorMax").style.display = 'block';

    document.getElementById("minimizesor").style.display = 'none';
    $(".demosor-tab").animate({
      width: '273%',
      right: '170%',

    });
    $('.table-msr').addClass("new-ht");

    // $('.demosor').addClass("plusbtn");    display: block;

    // $(".wl").removeClass('demosor');
  }
  closedemosorMax() {
    $('.table-msr').removeClass("new-ht");
    document.getElementById("ordershortcut").style.display = 'block';
    document.getElementById("tvchart").style.display = 'block';
    document.getElementById("tickerview").style.display = 'block';
    document.getElementById("tvchart").style.display = 'block';
    document.getElementById("dataTab").style.display = 'block';
    //document.getElementById("marketwatch").style.display = 'block';
    document.getElementById("maximizesor").style.display = 'block';
    document.getElementById("minimizesor").style.display = 'block';
    $(".demosor-tab").animate({

      height: '50%',
      width: '100%',
      right: '-1%',

    });
  }
  // restorewindow1() {
  //   $('.table-msr').removeClass("new-ht");
  //   document.getElementById("ordershortcut").style.display = 'block';
  //   document.getElementById("tvchart").style.display = 'block';
  //   document.getElementById("tickerview").style.display = 'block';
  //   document.getElementById("restoresor1").style.display = 'none';
  //   document.getElementById("marketwatch").style.display = 'block';
  //   document.getElementById("maximizesor").style.display = 'block';
  //   document.getElementById("minimizesor").style.display = 'block';
  //   $(".demosor").animate({

  //     height: '50%',
  //     width: '38%'

  //   });

  // }
  opendemosor() {
    document.getElementById("demosor").style.display = 'block';
  }
  closedemosor() {
    document.getElementById("demosor").style.display = 'none';
  }

  ngOnDestroy() {
    if (this.data.sourse3 != undefined) {
      this.data.sourse3.close();
    }

  }
}
