import { TestBed } from '@angular/core/testing';

import { OrdershortcutService } from './ordershortcut.service';

describe('OrdershortcutService', () => {
  let service: OrdershortcutService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OrdershortcutService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
