import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-marketwatch',
  templateUrl: './marketwatch.component.html',
  styleUrls: ['./marketwatch.component.css']
})

export class MarketwatchComponent implements OnInit {
public announcement:any;
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getannouncement();
  }
  closewindow(){
    document.getElementById("marketwatch").style.display='none'; 
  }
  openmarketwatch(){

    document.getElementById("marketwatch").style.display='block'; 
  }
  getannouncement(){
    this.http.get<any>('https://media.hashcashconsultants.com/wp-json/wp/v2/posts/?_embed&per_page=3')
    .subscribe(response => {
      this.announcement=response
console.log('///////////////',response);
    })
    //https://media.hashcashconsultants.com/wp-json/wp/v2/posts/?_embed&per_page=3
  }
}
