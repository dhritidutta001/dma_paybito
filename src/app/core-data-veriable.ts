export interface Login {
    email: string;
    password: string;
    userId: string;
    accessToken: any;
}
export interface Ticker {
    tradedata: [];
    offerdata: any;
}