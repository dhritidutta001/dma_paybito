import { Component, OnInit } from '@angular/core';
import { webSocket } from "rxjs/webSocket";

@Component({
  selector: 'app-charttwo',
  templateUrl: './charttwo.component.html',
  styleUrls: ['./charttwo.component.css']
})
export class CharttwoComponent implements OnInit {
  public marketdata:any
   private subject = webSocket("wss://ws.paxos.com/marketdata");
  //private subject = webSocket("wss://marketdata.staging-fix.itbit.com:6621/websocket");
  constructor() { }

  ngOnInit(): void {
   // this.subscribe();
  }
  closewatchtwo(){
    document.getElementById("tradingdata").style.display='none'; 
  }
  openwatchtwo(){

    document.getElementById("tradingdata").style.display='block'; 
  }
  // subscribe() {
  //   this.subject.subscribe(
  //     msg => {
  //       this.marketdata=msg;
  //      console.log('??????????????????????',this.marketdata);
       
  //       // use angular data pipe to push data into components that is subscribed on pipe
  //     },
  //     err => {
  //       console.log(err);
  //     }
  //    )
  // }

}
