import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CexioService {
  public baseURL="http://184.72.11.42:7503/BrokerApp/"

  constructor(private http: HttpClient) { }
GetAssets(){
  return  this.http.get<any>(this.baseURL+'publish/getAssetList/CEXIO')
}
  Getofferdata(data){
    // return  this.http.get<any>(this.baseURL+'publish/getAllOffersOnRegistID/'+userId);
    return this.http.post<any>(this.baseURL+'publish/getAllOffers',JSON.stringify(data),{ headers: { 'content-Type': 'application/json' } }); 
  }
  Getalltrade(data){
    return this.http.post<any>(this.baseURL+'publish/getAllTrades',JSON.stringify(data),{ headers: { 'content-Type': 'application/json' } }); 

  }
  BuyOffer(data){
    return this.http.post<any>(this.baseURL+'publish/createOrder',JSON.stringify(data),{ headers: { 'content-Type': 'application/json' } }); 
  }
  SellOffer(data){
    return this.http.post<any>(this.baseURL+'publish/createOrder',JSON.stringify(data),{ headers: { 'content-Type': 'application/json' } }); 
  }
  GetOfferstatus(data){
    return this.http.post<any>(this.baseURL+'publish/getOrderStatus',JSON.stringify(data),{ headers: { 'content-Type': 'application/json' } }); 
  
  }
  Getconnectionstatus(){
    return  this.http.get<any>(this.baseURL+'publish/getSessionStatus/cexio')

  }
}
