import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { CoreDataService } from '../core-data.service';
import {
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { OrderbookService } from '../../app/orderbook/orderbook.service';
import * as $ from 'jquery';
// import * as Stomp from 'stompjs';
// import * as SockJS from 'sockjs-client';
// import { webSocket } from "rxjs/webSocket";
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Subscription } from 'rxjs';
// import { CexioService } from '../services/cexio.service';
// import * as io from 'socket.io-client';

// const socket = io('http://localhost:3100');
//const socket = io('http://50.18.49.160:3000');
import { BinanceService } from '../services/binance.service';
import{CoinbaseproService} from '../services/coinbasepro.service'
import { MatExpansionPanel } from '@angular/material/expansion'
//const socket = io('http://184.72.11.42:7503/BrokerApp/publish/getSessionStatus/cexio');

@Component({
  selector: 'app-orderbook',
  templateUrl: './orderbook.component.html',
  styleUrls: ['./orderbook.component.css']
})
export class OrderbookComponent implements OnInit {
  // panelOpenState = false;

  // webSocketEndPoint: string = 'http://184.72.11.42:10080/cexMarketApi/ws';
  // topic: string = "/user/topic/orderbook";
  // paybitowebSocketEndPoint = 'http://184.72.11.42:10080/cexMarketApi/ws';
  // paybitotopic: string = "/user/topic/paybito/orderbook";
  // stompClient: any;
  // greeting: any;
  // biddata: any;
  shortcutdata = [];
  tickerdata = [];
  symbol: any;
  exchange: any;
  orderbook: any;
  alltickers: any;
  askdata: any;
  biddata: any;
  // public askdata: any;
  // askdataitbit: any;
  // biddataitbit: any;
  // reqNo: any;
  // addcolor: boolean = false;
  // asset: any;
  quantity: any;
  price: any;
   ordertype: any;
  assetprice: any;
  // assetquntity: any;
  assetpair: any;
   errormessage: any;
  cryptoasset: any;
  bidpricehighest: any;
  askpricelowest: any;
  askamtlowest: any;
  bidamthigest: any;
  assets: any;
  // msgType: any;
  exchangename: any;

  // private loginapi: Subscription;
  // private buyofferApi: Subscription;
  // private sellofferApi: Subscription;
  //private subject = webSocket("http://184.72.11.42:7503/BrokerApp/publish/GetSessionStatus/cexio");
  @ViewChild('matExpansionPanel', { static: true }) matExpansionPanelElement: MatExpansionPanel;

  constructor(public _CoinService:CoinbaseproService,public binanceService: BinanceService, private http: HttpClient, private modalService: NgbModal, public data: CoreDataService, public _OrderbookService: OrderbookService) {
    //this._connect();

  }

  ngOnInit(): void {
    //this._connectCEX();
    this.data.symbol = "XRPUSDT";
    this.exchange="binance";

    localStorage.setItem("symbol",this.symbol);
    this.data.SelectedBuyasset = this.data.symbol;
     this.data.SelectedSellasset = this.data.symbol;
     this.getshorcutdata();
    // this.cryptoasset = "BTC"
    // this.Getdata();
    //this.getdata();
    //this.Showasseets();
    // this.connectionstatus();
    //this.orderbook= this._BinanceService.Getorderbookdata();
    // this.or=this._BinanceService.orderbook;
    // this.askdata=this._BinanceService.askdata;


    // this.askdata = marketdata.askMarketDataList;
    //this.biddata = marketdata.bidMarketDataList;
  this.binanceService.SendSymboltoServer(this.data.symbol);
    this.createordertabletable();
    this.binanceService.Getorderbookdata();
    this.Getbinanceasset();
    
    this.matExpansionPanelElement.open();

    //console.log('BBBBBBBBBBBBBB', this.binanceService.askdata);
    // this.askpricelowest = this.binanceService.askdata[0].price;
    // this.askamtlowest = this.binanceService.askdata[0].quantity;
    // this.bidpricehighest =this.binanceService.biddata[0].price;
    //  this.bidamthigest = this.binanceService.biddata[0].quantity;

  }
  myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");

  }
  getshorcutdata() {
    for (var i = 0; i < this.binanceService.shortcutasset.length; i++) {
      var selectedasset = this.data.symbol;
      if (selectedasset == this.binanceService.shortcutasset[i].symbol) {
        this.data.Lasttraderprice = this.binanceService.shortcutasset[i].currentClose;
      }

    }
  }
//   filterFunction(){
//     debugger;
// //    document.getElementById("myDropdown").classList.toggle("show");

//     var input, filter, ul, li, a, i;
//     input = document.getElementById("myInput");
//     filter = input.value.toUpperCase();
//    li = document.getElementById("myDropdown");
//     a = li.getElementsByTagName("a");
//     for (i = 0; i < a.length; i++) {
//    var   txtValue = a[i].textContent || a[i].innerText;
//       if (txtValue.toUpperCase().indexOf(filter) > -1) {
//         li[i].style.display = "";
//       } else {
//         li[i].style.display = "none";
//       }
//     }
//   }
  filterFunction(){
 //   debugger;
//    document.getElementById("myDropdown").classList.toggle("show");

    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput1");
    filter = input.value.toUpperCase();
   var div = document.getElementById("myDropdown");
    a = div.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
   var   txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = "";
      } else {
        a[i].style.display = "none";
      }
    }
  }


  // myFunction() {

  //   var input, filter, table, tr, td, i, txtValue;
  //   input = document.getElementById("myInput");

  //   // if(input==null){
  //   //   $(".table-msr").removeClass("search-msr");
  //   // }
  //   filter = input.value.toUpperCase();
  //   table = document.getElementById("table-msr");
  //   tr = table.getElementsByTagName("tr");
  //   for (i = 0; i < tr.length; i++) {
  //     td = tr[i].getElementsByTagName("td")[0];
  //     if (td) {
  //       txtValue = td.textContent || td.innerText;
  //       if (txtValue.toUpperCase().indexOf(filter) > -1) {
  //         tr[i].style.display = "";
  //       } else {
  //         tr[i].style.display = "none";
  //       }
  //     }
  //   }
  //   $(".table-msr").addClass("search-msr");
  // }
  Getasset(asset){
 //  alert();
  //debugger;
 // alert(this.exchange);
  var symbol=asset;
 
  this.data.tempsearch=symbol;

    localStorage.setItem("symbol",this.symbol);
 
    if (this.exchange == "binance" || this.exchange == "") {
   
      this.data.symbol=symbol;
      this.createordertabletable();
      }
      else if (this.exchange =='Coinbasepro') {
     //   alert('sdfsd');
        this.data.symbol=symbol;
        this.data.SelectedBuyasset = this.data.symbol;
     this.data.SelectedSellasset = this.data.symbol;
        this.createCoinbasetable();
      }
   
    // if (this.exchange == "Binance" || this.exchange == undefined) {
    // this.Getbinanceasset();
    // }
    // else if (this.exchange == 'Coinbasepro') {
    //   this.Getcoinbaseasset();
    // }
    //this.data.symbol="";
  
    document.getElementById("myDropdown").classList.toggle("show");
  }
  createordertabletable() {
    var symbol=this.data.symbol;
    this.binanceService.getsnapshot(symbol);
     // this.askpricelowest = this.binanceService.askdata[0].price;
    // this.askamtlowest = this.binanceService.askdata[0].quantity;
    // this.bidpricehighest =this.binanceService.biddata[0].price;
    //  this.bidamthigest = this.binanceService.biddata[0].quantity;
    // this.binanceService.getsnapshot(symbol)
    //   .subscribe(data => {
    //   if(data !=""){
    //     this.askdata = data.asks;
    //     this.biddata = data.bids;
    //   //  alert(this.askdata);
    //     this.askpricelowest = this.askdata[0][0];
    //     this.askamtlowest = this.askdata[0][1];
    //     this.bidpricehighest = this.biddata[0][0];
    //     this.bidamthigest = this.biddata[0][1];
    //     var lastupdateId = data.lastUpdateId;
    //     // console.log('snapshot---------', this.askdata);
    //     var orderdata = this.binanceService.orderbook;
    //     console.log('snapshot---------', orderdata);
    //     var streamask = this.binanceService.stremaskdata;
    //     var streamdiddata = this.binanceService.strembiddata;
  
    //     if (orderdata.firstUpdateId <= lastupdateId + 1 && orderdata.lastUpdateId >= lastupdateId + 1) {
    //       // alert('hgjhfjhf');
    //       // this.askdata.push({})
    //       for (var i = 0; i < this.askdata.length; i++) {
    //         var price = this.askdata[i][0];
    //         var quantity = this.askdata[i][1];
    //         for (var x = 0; x < streamask.length; x++) {
    //           if (streamask[i].price = price) {
    //             this.askdata[i][1] = streamask[x].quantity;
    //           }
    //           else {
    //             var price = streamask[x].price;
    //             var quantity = streamask[x].quantity;
    //             this.askdata[i].push({ price, quantity });
    //           }
    //         }

    //       }
    //       for (var i = 0; i < this.biddata.length; i++) {
    //         var price = this.biddata[i][0];
    //         var quantity = this.biddata[i][1];
    //         for (var x = 0; x < streamdiddata.length; x++) {
    //           if (streamdiddata[i].price = price) {
    //             this.biddata[i][1] = streamdiddata[x].quantity;
    //           }
    //           else {
    //             var price = streamdiddata[x].price;
    //             var quantity = streamdiddata[x].quantity;
    //             this.biddata[i].push({ price, quantity });
    //           }
    //         }

    //       }
    //     }
    //   }
     

    //   })
  }
  createCoinbasetable(){
  //  alert('coin');
    var symbol=this.data.symbol;
    this.binanceService.askdata="";
    this.binanceService.biddata="";
    this._CoinService.getsnapshot(symbol)
    .subscribe(data => {
      this.binanceService.askdata=data.asks;
      this.binanceService.biddata=data.bids;
      console.log('>>>>>>>>>',data.asks);
       this.askpricelowest = this.binanceService.askdata[0].price;
       this.bidpricehighest =this.binanceService.biddata[0].price;
    this.askamtlowest = "";
  
     this.bidamthigest = "";

    })
  }
  // subscribe() {
  //   debugger;
  //   this.subject.subscribe(
  //     msg => {
  //       var itbitdata=JSON.stringify(msg);
  //       var OB=JSON.parse(itbitdata);
  //       console.log('??????????????????????dhriti',OB);
  //     //  if(OB.type='SNAPSHOT'){
  //     //   this.askdataitbit=OB.asks;
  //     //   this.biddataitbit=OB.bids;
  //     //   console.log('??????????????????????dhriti',this.askdataitbit);
  //     //  }
  //     //  else if(OB.type='UPDATE'){

  //     //  }

  //      //console.log('??????????????????????dhriti',OB.bids);

  //       // use angular data pipe to push data into components that is subscribed on pipe
  //     },
  //     err => {
  //       console.log.ge(err);
  //     }
  //    )
  // }

  // public changeSuccessMessage() {
  //   this._success.next(`${new Date()} - Message successfully changed.`);
  // }
  Getbinanceasset() {
    this.binanceService.Getallassets()
      .subscribe(data => {
        this.assets = JSON.parse(JSON.stringify(data.symbols))
      //  console.log('assets--------------', this.assets);

      })
  }
  Getcoinbaseasset(){
  //  alert('32423');
    this._CoinService.Getallassets()
    .subscribe(data => {
      this.assets = JSON.parse(JSON.stringify(data))
   console.log('assetscoinbase--------------', this.assets[0].id);

    })
  }

  closewindow() {
    document.getElementById("daataTab").style.display = 'none';
  }
  opendatawindow() {

    document.getElementById("daataTab").style.display = 'block';
  }
  showtableone() {
    document.getElementById("table1").style.display = 'block';
    document.getElementById("table2").style.display = 'none';
  }
  showtabletwo() {
    document.getElementById("table2").style.display = 'block';
    document.getElementById("table1").style.display = 'none';
  }
  // Getdata() {
  //    //alert('aaaaaaaaaa');
  //   // socket.on('datak', (res) => {
  //   //   var test1 = res.msg;
  //   //   console.log('7777777777777777', test1)

  //   // })
  //   //alert('rrrrrrrr');
  //   socket.on('Order', (res) => {
  //     this.orderbook = res.msg;
  //     this.askdata=this.orderbook.askDepthDelta;
  //     this.biddata=this.orderbook.bidDepthDelta;
  //    console.log('8888888888888', this.orderbook)

  //   })

  // }
  // Getticker(){

  //   socket.on('allticker', (res) => {
  //     this.alltickers= res.msg;
  //    console.log('99999999999', this.alltickers)

  //   })

  // }
  //   getdata() {
  //   socket.on('data1', (res) => {
  //     this.tickerdata = res.msg;
  //     //  var tdata=JSON.parse(data);
  //     // this.tickerdata=tdata.data
  //     // console.log('ddddddddddddddddddddddddd',this.tickerdata);
  //     //localStorage.setItem('ticker', this.tickerdata);
  //     // this.data.Lasttraderprice = this.tickerdata.last;;

  //   })

  //   socket.on('data2', (res) => {
  //     var data = res.msg;

  //     var symbolone = data.symbol1;
  //     var symboltwo = data.symbol2;

  //     // if(this.shortcutdata.length<=2){
  //     this.shortcutdata.push({
  //       open24: data.open24,
  //       price: data.price,
  //       symbol1: data.symbol1,
  //       symbol2: data.symbol2,
  //       volume: data.volume

  //     })
  //     console.log('SSSSSSSSSSSS',this.shortcutdata);
  //     var newassetpair = data.symbol1 + data.symbol2;
  //     var selectedasset = this.data.SelectedBuyasset + this.data.SelectedSellasset;
  //     if (newassetpair == selectedasset) {

  //       this.data.Lasttraderprice = data.price;
  //       // alert( this.data.Lasttraderprice);
  //     }
  //     // }
  //     if (this.shortcutdata.length > 2) {
  //       for (var i = 0; i < this.shortcutdata.length; i++) {

  //         if (symbolone == this.shortcutdata[i].symbol1 && symboltwo == this.shortcutdata[i].symbol2) {
  //           // this.shortcutdata.splice(i,5);

  //           this.shortcutdata[i].price = data.price;


  //         }

  //         var selectedasset = this.data.SelectedBuyasset + this.data.SelectedSellasset;

  //         var assetpair = this.shortcutdata[i].symbol1 + this.shortcutdata[i].symbol2;
  //         if (selectedasset == assetpair) {

  //           this.data.Lasttraderprice = this.shortcutdata[i].price;
  //           console.log('SSSSSSSSSSSS',this.shortcutdata);
  //           // alert( this.data.Lasttraderprice);
  //         }
  //       }

  //     }


  //   })

  // }
  // warnKyc(content) {
  //   this.modalService.open(content, {
  //   windowClass: 'dark-modal' 
  //   });
  // }
  // reason: any;
  // icon: any;
  testalert() {

    this.data.alert('alert for success', 'success');
    this.data.alert('PLEASE SUBMIT DOCUMENTS, ENSURE THAT ALL DOCUMENTS ARE IN JPG OR JPEG FORMAT. PNG, GIF, and other formats are not permitted.', 'info');
  }

  dismiss() {
    $('.alertPlace').remove().fadeOut();
  }
  // warnKyc(content) {
  //   this.modalService.open(content, { centered: true, windowClass: 'dark-modal' });
  // }

  warns(sellconfirm, price) {
    this.assetprice = price;
    var userstatus = localStorage.getItem('userstatus');
    var userId = localStorage.getItem('userId');
    this.assetpair = localStorage.getItem('symbol');
    // alert(userId);
    if (userId == null) {
      this.modalService.open(sellconfirm, {
        centered: true
      });
    }
    else {
      this.data.alert('Please Login to place sell offer', 'danger');
    }

  }
  warnb(buyconfirm, price) {
    this.assetprice = price;
    this.assetpair = localStorage.getItem('symbol');
    var userId = localStorage.getItem('userId');
    if (userId == null) {
      this.modalService.open(buyconfirm, {
        centered: true
      });
    }
    else {
      this.data.alert('Please Login to place buy offer', 'danger');
    }

  }
  warn(buyconfirm) {
    // this.modalService.open(buyconfirm, {
    //   centered: true
    // });
  }
  // _connectCEX() {
  //   // console.log("Initialize WebSocket Connection");
  //   let ws = new SockJS(this.webSocketEndPoint);
  //   this.stompClient = Stomp.over(ws);
  //   const _this = this;
  //   _this.stompClient.connect({}, function (frame) {
  //     _this.stompClient.subscribe(_this.topic, function (sdkEvent) {
  //       _this.onMessageReceivedorderbookdata(sdkEvent);
  //       //  _this.onMessageReceivedbid(sdkEvent);
  //       // console.log('sdkEvent/////',sdkEvent);

  //     });
  //     //_this.stompClient.reconnect_delay = 2000;
  //   }, this.errorCallBack);
  // };
  // _connectPAYBITO() {
  //   console.log("Initialize paybito WebSocket Connection");
  //   let ws = new SockJS(this.paybitowebSocketEndPoint);
  //   this.stompClient = Stomp.over(ws);
  //   const _this = this;
  //   _this.stompClient.connect({}, function (frame) {
  //     _this.stompClient.subscribe(_this.paybitotopic, function (sdkEvent) {
  //       _this.onMessageReceivedorderbookdataPaybito(sdkEvent);
  //       //  _this.onMessageReceivedbid(sdkEvent);
  //       // console.log('sdkEvent/////',sdkEvent);

  //     });
  //     //_this.stompClient.reconnect_delay = 2000;
  //   }, this.errorCallBack);
  // };
  // _disconnect() {
  //   if (this.stompClient !== null) {
  //     this.stompClient.disconnect();
  //   }
  //   console.log("Disconnected");
  // }
  // _disconnectPAYBITO() {
  //   if (this.stompClient !== null) {
  //     this.stompClient.disconnect();
  //   }
  //   console.log("Disconnected");
  // }
  // errorCallBack(error) {
  //   console.log("errorCallBack -> " + error)
  //   setTimeout(() => {
  //     this._connectCEX();
  //   }, 5000);
  // }
  // sendmessageCEXorderbook() {

  //   this.reqNo = Math.floor((Math.random() * 1000) + 1);
  //   const req = {
  //     "symbol": this.symbol,
  //     "msgType": "market_Data_Request",
  //     "subscriptionRequestType": "1",
  //     "marketDepth": "2",
  //     "mdReqID": "111",
  //     "mdEntryTypeBid": "0",
  //     "mdEntryTypeAsk": "1",
  //     "noMDEntryTypes": "2",
  //     "noRelatedSym": "1",
  //     "mdUpdateType": "1"
  //   };
  //   localStorage.setItem('reqno', this.reqNo);
  //   localStorage.setItem('symbol', this.symbol);
  //   this._sendAsk(req);
  // }
  // sendmessagePAYBITOorderbook() {

  //   this.reqNo = Math.floor((Math.random() * 1000) + 1);
  //   const req = {
  //     "symbol": this.symbol,
  //     "msgType": "market_Data_Request",
  //     "subscriptionRequestType": "1",
  //     "marketDepth": "2",
  //     "mdReqID": "112",
  //     "mdEntryTypeBid": "0",
  //     "mdEntryTypeAsk": "1",
  //     "noMDEntryTypes": "2",
  //     "noRelatedSym": "1",
  //     "mdUpdateType": "1"
  //   };
  //   localStorage.setItem('reqno', this.reqNo);
  //   localStorage.setItem('symbol', this.symbol);
  //   this._sendrequestPAYBITO(req);
  // }
  // sendmessageCEXBid() {
  //   const req = {
  //     "symbol": 'BTC/USD',
  //     "msgType": "market_Data_Request",
  //     "subscriptionRequestType": "1",
  //     "marketDepth": "2",
  //     "mdReqID": "3131",
  //     "mdEntryType": "0",
  //     "noMDEntryTypes": "1",
  //     "noRelatedSym": "1",
  //     "mdUpdateType": "1"
  //   };
  //   this._sendBid(req);

  // }

  // _sendBid(message) {
  //   // console.log("calling logout api via web socket");
  //   //console.log(message);
  //   this.stompClient.send("/app/sendRequest", {}, JSON.stringify(message));
  // }

  // onMessageReceivedorderbookdata(message) {

  //   var str = JSON.stringify(message.body);
  //   var obj = JSON.parse(str);
  //   var marketdata = JSON.parse(obj);
  //   // console.log("Message Recieved from Server :: " + marketdata.messageType);
  //   // var test=marketdata.marketDataList;
  //   // this.askdata=marketdata.marketDataList;
  //   if (marketdata != null && marketdata != "") {

  //     if (marketdata.messageType == 'W') {

  //       // this.askdata = marketdata.askMarketDataList;
  //       // this.biddata = marketdata.bidMarketDataList;
  //       this.askdataitbit = marketdata.askMarketDataList;
  //       this.biddataitbit = marketdata.bidMarketDataList;

  //       this.askdataitbit.sort();
  //       this.biddataitbit.sort(function (a, b) { return a.mdEntryPrice - b.mdEntryPrice });
  //       this.biddataitbit.reverse();
  //       //   this.biddataitbit.sort();
  //       // console.log('MMMMMMMMMMMMMMM',this.askdataitbit.length);
  //     }
  //     else if (marketdata.messageType == 'X') {

  //       // for (var j = 0; j < marketdata.askMarketDataList.length; j++) {
  //       //   if (marketdata.askMarketDataList[j].mdUpdateAction == "1") {
  //       //     var Price = marketdata.askMarketDataList[j].mdEntryPrice;
  //       //     var quantity = marketdata.askMarketDataList[j].mdEntryQuantity;
  //       //     for (var i = 0; i < this.askdataitbit.length; i++) {
  //       //       if (this.askdataitbit[i].mdEntryPrice == Price) {
  //       //         this.addcolor = true;

  //       //         this.askdataitbit[i].mdEntryQuantity = quantity;

  //       //       }
  //       //       this.askdataitbit.sort();
  //       //     }
  //       //   }
  //       //   if (marketdata.askMarketDataList[j].mdUpdateAction == "0") {

  //       //     this.askdataitbit.push({
  //       //       messageType: marketdata.askMarketDataList[j].messageType,
  //       //       symbol: marketdata.askMarketDataList[j].symbol,
  //       //       sendingTime: marketdata.askMarketDataList[j].sendingTime,
  //       //       mdReqId: marketdata.askMarketDataList[j].mdEntryPrice,
  //       //       mdEntryPrice: marketdata.askMarketDataList[j].mdEntryPrice,
  //       //       mdEntryQuantity: marketdata.askMarketDataList[j].mdEntryQuantity,
  //       //       mdEntryType: marketdata.askMarketDataList[j].mdEntryType,
  //       //       mdUpdateAction: marketdata.askMarketDataList[j].mdUpdateAction

  //       //     })
  //       //     this.askdataitbit.sort();

  //       //   }
  //       //   if (marketdata.askMarketDataList[j].mdUpdateAction == "2") {
  //       //     var Price = marketdata.askMarketDataList[j].mdEntryPrice;
  //       //     var quantity = marketdata.askMarketDataList[j].mdEntryQuantity;
  //       //     for (var i = 0; i < this.askdataitbit.length; i++) {
  //       //       if (this.askdataitbit[i].mdEntryPrice == Price) {
  //       //         this.askdataitbit.splice(i, 1);
  //       //         this.askdataitbit.sort();
  //       //       }
  //       //     }
  //       //    // console.log('LLLLLLLLLLLLLLLLL', this.askdataitbit.length);

  //       //   }

  //       // }
  //       for (var j = 0; j < marketdata.askMarketDataList.length; j++) {
  //         if (marketdata.askMarketDataList[j].mdUpdateAction == "1") {
  //           var Price = marketdata.askMarketDataList[j].mdEntryPrice;
  //           var quantity = marketdata.askMarketDataList[j].mdEntryQuantity;
  //           for (var i = 0; i < this.askdataitbit.length; i++) {
  //             if (this.askdataitbit[i].mdEntryPrice == Price) {

  //               this.askdataitbit[i].mdEntryQuantity = quantity;


  //             }
  //             this.askdataitbit.sort();

  //           }
  //         }
  //         if (marketdata.askMarketDataList[j].mdUpdateAction == "0") {

  //           this.askdataitbit.push({
  //             messageType: marketdata.askMarketDataList[j].messageType,
  //             symbol: marketdata.askMarketDataList[j].symbol,
  //             sendingTime: marketdata.askMarketDataList[j].sendingTime,
  //             mdReqId: marketdata.askMarketDataList[j].mdEntryPrice,
  //             mdEntryPrice: marketdata.askMarketDataList[j].mdEntryPrice,
  //             mdEntryQuantity: marketdata.askMarketDataList[j].mdEntryQuantity,
  //             mdEntryType: marketdata.askMarketDataList[j].mdEntryType,
  //             mdUpdateAction: marketdata.askMarketDataList[j].mdUpdateAction

  //           })
  //           this.askdataitbit.sort();


  //         }
  //         if (marketdata.askMarketDataList[j].mdUpdateAction == "2") {
  //           var Price = marketdata.askMarketDataList[j].mdEntryPrice;
  //           var quantity = marketdata.askMarketDataList[j].mdEntryQuantity;
  //           for (var i = 0; i < this.askdataitbit.length; i++) {
  //             if (this.askdataitbit[i].mdEntryPrice == Price) {
  //               this.askdataitbit.splice(i, 1);
  //               this.askdataitbit.sort();

  //             }
  //           }

  //         }

  //       }
  //       this.askpricelowest = this.askdataitbit[0].mdEntryPrice;
  //       this.askamtlowest = this.askdataitbit[0].mdEntryQuantity;
  //       for (var j = 0; j < marketdata.bidMarketDataList.length; j++) {
  //         if (marketdata.bidMarketDataList[j].mdUpdateAction == "1") {
  //           var Price = marketdata.bidMarketDataList[j].mdEntryPrice;
  //           var quantity = marketdata.bidMarketDataList[j].mdEntryQuantity;
  //           for (var i = 0; i < this.biddataitbit.length; i++) {
  //             if (this.biddataitbit[i].mdEntryPrice == Price) {

  //               this.biddataitbit[i].mdEntryQuantity = quantity;


  //             }
  //             //this.biddataitbit.sort();
  //             //this.biddataitbit.reverse();
  //             this.biddataitbit.sort(function (a, b) { return a.mdEntryPrice - b.mdEntryPrice });
  //             this.biddataitbit.reverse();
  //           }
  //         }
  //         if (marketdata.bidMarketDataList[j].mdUpdateAction == "0") {

  //           this.biddataitbit.push({
  //             messageType: marketdata.bidMarketDataList[j].messageType,
  //             symbol: marketdata.bidMarketDataList[j].symbol,
  //             sendingTime: marketdata.bidMarketDataList[j].sendingTime,
  //             mdReqId: marketdata.bidMarketDataList[j].mdEntryPrice,
  //             mdEntryPrice: marketdata.bidMarketDataList[j].mdEntryPrice,
  //             mdEntryQuantity: marketdata.bidMarketDataList[j].mdEntryQuantity,
  //             mdEntryType: marketdata.bidMarketDataList[j].mdEntryType,
  //             mdUpdateAction: marketdata.bidMarketDataList[j].mdUpdateAction

  //           })
  //           //  this.biddataitbit.sort();
  //           // this.biddataitbit.reverse();
  //           this.biddataitbit.sort(function (a, b) { return a.mdEntryPrice - b.mdEntryPrice });
  //           this.biddataitbit.reverse();
  //         }
  //         if (marketdata.bidMarketDataList[j].mdUpdateAction == "2") {
  //           var Price = marketdata.bidMarketDataList[j].mdEntryPrice;
  //           var quantity = marketdata.bidMarketDataList[j].mdEntryQuantity;
  //           for (var i = 0; i < this.biddataitbit.length; i++) {
  //             if (this.biddataitbit[i].mdEntryPrice == Price) {
  //               this.biddataitbit.splice(i, 1);
  //               //this.biddataitbit.sort();
  //               this.biddataitbit.sort(function (a, b) { return a.mdEntryPrice - b.mdEntryPrice });
  //               this.biddataitbit.reverse();
  //             }
  //           }

  //         }
  //         // this.biddataitbit.reverse();
  //         //  this.biddataitbit.sort();
  //         this.biddataitbit.sort(function (a, b) { return a.mdEntryPrice - b.mdEntryPrice });
  //         this.biddataitbit.reverse();
  //       }
  //       this.bidpricehighest = this.biddataitbit[0].mdEntryPrice;
  //       this.bidamthigest = this.biddataitbit[0].mdEntryQuantity;
  //     }

  //   }

  // }
  // onMessageReceivedorderbookdataPaybito(message) {

  //   var str = JSON.stringify(message.body);
  //   var obj = JSON.parse(str);
  //   var marketdata = JSON.parse(obj);
  //   console.log("Message Recieved from Server :: " + marketdata.messageType);
  //   // var test=marketdata.marketDataList;
  //   // this.askdata=marketdata.marketDataList;
  //   if (marketdata != null && marketdata != "") {

  //     if (marketdata.messageType == 'W') {

  //       // this.askdata = marketdata.askMarketDataList;
  //       // this.biddata = marketdata.bidMarketDataList;
  //       this.askdataitbit = marketdata.askMarketDataList;
  //       this.biddataitbit = marketdata.bidMarketDataList;
  //       //alert(this.askdataitbit+ this.biddataitbit )
  //       this.askdataitbit.sort();
  //       this.biddataitbit.sort(function (a, b) { return a.mdEntryPrice - b.mdEntryPrice });
  //       this.biddataitbit.reverse();
  //       this.askpricelowest = this.askdataitbit[0].mdEntryPrice;
  //       this.askamtlowest = this.askdataitbit[0].mdEntryQuantity;
  //       this.bidpricehighest = this.biddataitbit[0].mdEntryPrice;
  //       this.bidamthigest = this.biddataitbit[0].mdEntryQuantity;
  //       //   this.biddataitbit.sort();
  //       // console.log('MMMMMMMMMMMMMMM',this.askdataitbit.length);
  //     }
  //     else if (marketdata.messageType == 'X') {

  //       for (var j = 0; j < marketdata.askMarketDataList.length; j++) {
  //         if (marketdata.askMarketDataList[j].mdUpdateAction == "1") {
  //           var Price = marketdata.askMarketDataList[j].mdEntryPrice;
  //           var quantity = marketdata.askMarketDataList[j].mdEntryQuantity;
  //           for (var i = 0; i < this.askdataitbit.length; i++) {
  //             if (this.askdataitbit[i].mdEntryPrice == Price) {

  //               this.askdataitbit[i].mdEntryQuantity = quantity;

  //             }
  //             this.askdataitbit.sort();

  //           }
  //         }
  //         if (marketdata.askMarketDataList[j].mdUpdateAction == "0") {

  //           this.askdataitbit.push({
  //             messageType: marketdata.askMarketDataList[j].messageType,
  //             symbol: marketdata.askMarketDataList[j].symbol,
  //             sendingTime: marketdata.askMarketDataList[j].sendingTime,
  //             mdReqId: marketdata.askMarketDataList[j].mdEntryPrice,
  //             mdEntryPrice: marketdata.askMarketDataList[j].mdEntryPrice,
  //             mdEntryQuantity: marketdata.askMarketDataList[j].mdEntryQuantity,
  //             mdEntryType: marketdata.askMarketDataList[j].mdEntryType,
  //             mdUpdateAction: marketdata.askMarketDataList[j].mdUpdateAction

  //           })
  //           this.askdataitbit.sort();


  //         }
  //         if (marketdata.askMarketDataList[j].mdUpdateAction == "2") {
  //           var Price = marketdata.askMarketDataList[j].mdEntryPrice;
  //           var quantity = marketdata.askMarketDataList[j].mdEntryQuantity;
  //           for (var i = 0; i < this.askdataitbit.length; i++) {
  //             if (this.askdataitbit[i].mdEntryPrice == Price) {
  //               this.askdataitbit.splice(i, 1);
  //               this.askdataitbit.sort();

  //             }
  //           }

  //         }

  //       }
  //       this.askpricelowest = this.askdataitbit[0].mdEntryPrice;
  //       this.askamtlowest = this.askdataitbit[0].mdEntryQuantity;
  //       for (var j = 0; j < marketdata.bidMarketDataList.length; j++) {
  //         if (marketdata.bidMarketDataList[j].mdUpdateAction == "1") {
  //           var Price = marketdata.bidMarketDataList[j].mdEntryPrice;
  //           var quantity = marketdata.bidMarketDataList[j].mdEntryQuantity;
  //           for (var i = 0; i < this.biddataitbit.length; i++) {
  //             if (this.biddataitbit[i].mdEntryPrice == Price) {

  //               this.biddataitbit[i].mdEntryQuantity = quantity;


  //             }
  //             //this.biddataitbit.sort();
  //             //this.biddataitbit.reverse();
  //             this.biddataitbit.sort(function (a, b) { return a.mdEntryPrice - b.mdEntryPrice });
  //             this.biddataitbit.reverse();
  //           }
  //         }
  //         if (marketdata.bidMarketDataList[j].mdUpdateAction == "0") {

  //           this.biddataitbit.push({
  //             messageType: marketdata.bidMarketDataList[j].messageType,
  //             symbol: marketdata.bidMarketDataList[j].symbol,
  //             sendingTime: marketdata.bidMarketDataList[j].sendingTime,
  //             mdReqId: marketdata.bidMarketDataList[j].mdEntryPrice,
  //             mdEntryPrice: marketdata.bidMarketDataList[j].mdEntryPrice,
  //             mdEntryQuantity: marketdata.bidMarketDataList[j].mdEntryQuantity,
  //             mdEntryType: marketdata.bidMarketDataList[j].mdEntryType,
  //             mdUpdateAction: marketdata.bidMarketDataList[j].mdUpdateAction

  //           })
  //           //  this.biddataitbit.sort();
  //           // this.biddataitbit.reverse();
  //           this.biddataitbit.sort(function (a, b) { return a.mdEntryPrice - b.mdEntryPrice });
  //           this.biddataitbit.reverse();
  //         }
  //         if (marketdata.bidMarketDataList[j].mdUpdateAction == "2") {
  //           var Price = marketdata.bidMarketDataList[j].mdEntryPrice;
  //           var quantity = marketdata.bidMarketDataList[j].mdEntryQuantity;
  //           for (var i = 0; i < this.biddataitbit.length; i++) {
  //             if (this.biddataitbit[i].mdEntryPrice == Price) {
  //               this.biddataitbit.splice(i, 1);
  //               //this.biddataitbit.sort();
  //               this.biddataitbit.sort(function (a, b) { return a.mdEntryPrice - b.mdEntryPrice });
  //               this.biddataitbit.reverse();
  //             }
  //           }

  //         }
  //         // this.biddataitbit.reverse();
  //         //  this.biddataitbit.sort();
  //         this.biddataitbit.sort(function (a, b) { return a.mdEntryPrice - b.mdEntryPrice });
  //         this.biddataitbit.reverse();
  //       }
  //       this.bidpricehighest = this.biddataitbit[0].mdEntryPrice;
  //       this.bidamthigest = this.biddataitbit[0].mdEntryQuantity;
  //     }

  //   }

  // }
  // onMessageReceivedbid(message) {

  //   var str = JSON.stringify(message.body);
  //   var obj = JSON.parse(str);
  //   var marketdata = JSON.parse(obj);
  //   //console.log("Message Recieved from Server :: " + marketdata.marketDataList);
  //   // this.biddata=marketdata.marketDataList;
  //   if (marketdata != null && marketdata != "") {
  //     for (var i = 0; i < marketdata.marketDataList.length; i++) {
  //       if (marketdata.marketDataList[0].mdEntryType == 0) {
  //         if (marketdata.marketDataList[i].messageType == "W") {
  //           this.biddata = marketdata.marketDataList;
  //         }
  //         else if (marketdata.marketDataList[i].messageType == "X") {
  //           if (marketdata.marketDataList[i].mdUpdateAction == "2") {
  //             var Price = marketdata.marketDataList[i].mdEntryPrice;
  //             var quantity = marketdata.marketDataList[i].mdEntryQuantity;
  //             for (var j = 0; j < this.biddata.length; j++) {

  //               if (this.biddata[j].mdEntryPrice == Price) {
  //                 this.biddata.push({
  //                   messageType: 'W',
  //                   symbol: this.biddata[j].symbol,
  //                   sendingTime: this.biddata[j].mdEntryPrice,
  //                   mdReqId: this.biddata[j].mdEntryPrice,
  //                   mdEntryPrice: Price,
  //                   mdEntryQuantity: quantity,
  //                   mdEntryType: null,
  //                   mdUpdateAction: null

  //                 })
  //               }
  //             }
  //           }
  //           else if (marketdata.marketDataList[i].mdUpdateAction == "0") {
  //             this.biddata.push({
  //               messageType: 'W',
  //               symbol: marketdata.marketDataList[i].symbol,
  //               sendingTime: marketdata.marketDataList[i].mdEntryPrice,
  //               mdReqId: marketdata.marketDataList[i].mdEntryPrice,
  //               mdEntryPrice: marketdata.marketDataList[i].mdEntryPrice,
  //               mdEntryQuantity: marketdata.marketDataList[i].mdEntryQuantity,
  //               mdEntryType: null,
  //               mdUpdateAction: null
  //             })

  //           }
  //         }
  //       }
  //     }
  //   }
  //   console.log("Message Recieved from Server  biddata:: " + this.biddata.length);
  // }
  // handleMessage(message) {
  //   this.greeting = message;
  // }


  Showasseets() {
    // this._CexioService.GetAssets()
    //   .subscribe(data => {
    //     this.assets = data.assetList;
    //     //  console.log('=================================///////////////////////////', data.assetList);
    //   })
  }
  // getAsset(event) {
  //   // debugger;
  //   var assetpair = event;
  //   this.symbol = assetpair;
  //   this.data.symbol=this.symbol;
  //   localStorage.setItem("symbol",this.symbol);
  //   this.createordertabletable();
   
  //   if (this.exchange == "cex.io" || this.exchange == undefined) {
    
  //   }
  //   else if (this.exchange = 'Itbit') {
     
  //   }

  //   // console.log('////////////////dddddd',this.cryptoasset);
  // }
  getExchange(ex) {
 //   debugger;
    this.exchange = ex;
    this.data.symbol ="";
    if (this.exchange == "Binance" || this.exchange == undefined) {
      this.Getbinanceasset();
      this.data.symbol = "XRPUSDT";
      this.createordertabletable();
      }
      else if (this.exchange == 'Coinbasepro') {

        this.Getcoinbaseasset();
        this.data.symbol = "XRP-USD";
        this.data.SelectedBuyasset = this.data.symbol;
     this.data.SelectedSellasset = this.data.symbol;
        this.createCoinbasetable();
      }
  }
  getselloffertype(event) {
    //alert(event);
    this.ordertype = event;
    if (this.ordertype == 1) {
      document.getElementById('slprice').style.display = 'none';
      document.getElementById('smprice').style.display = 'block';
      //document.getElementById('oprice').style.
    }
    else if (this.ordertype == 2) {
      document.getElementById('slprice').style.display = 'block';
      document.getElementById('smprice').style.display = 'none';
    }

   }
  getoffertype(event) {
    //alert(event);
    this.ordertype = event;
    if (this.ordertype == 1) {
      document.getElementById('lprice').style.display = 'none';
      document.getElementById('mprice').style.display = 'block';
      //document.getElementById('oprice').style.
    }
    else if (this.ordertype == 2) {
      document.getElementById('lprice').style.display = 'block';
      document.getElementById('mprice').style.display = 'none';
    }

  }
  buyoffer() {
      //var buyofferobj = {};
      var symbol= this.symbol;
      var side = 'BUY';
      var quantity = this.quantity;
      var timinforce='GTC'
      if (this.ordertype == 1 || this.ordertype == undefined) {
        var type='MARKET';
        var price ="";

      }
      else if (this.ordertype == 2) {
        //  alert(this.price);
         type='LIMIT';
           price =this.price;
        if (this.price == "" || this.price == undefined) {
          this.data.alert('Mandatory field missing!', 'danger');
        }

      }
      if (this.ordertype == undefined) {
        var type='MARKET';
      }
      else {
        var type='LIMIT';
      }

      //buyofferobj['msgType'] = 'order_single';
      var userId = localStorage.getItem('userId');
    //  buyofferobj['registID'] = userId;
    //  var jsonString = JSON.stringify(buyofferobj);
      // this.deviceinfoApi=this.http.post<any>(this.data.WEBSERVICE+'/user/deviceVerification',jsonString,{headers: { 'content-Type': 'application/json'}})
      this.binanceService.Sellorder(symbol,side,type,timinforce,quantity,price)
      .subscribe(response => {
        if (response !=null) {
        var responsedata=response;
        if(responsedata.status=='FILLED')
        console.log('---------',responsedata);
        this.data.alert('order placed successfully', 'success');
        }
    });
  }
  
  selloffer() {
     //var buyofferobj = {};
    var symbol = this.symbol;
    var side = 'SELL';
    var quantity= this.quantity;
    var timinforce='GTC';
    if (this.ordertype == 1 || this.ordertype == undefined) {
      var type='MARKET';
      var price ="";
    }
    else if (this.ordertype == 2) {
       price = this.price;
       type="LIMIT"
      if (this.price == "" || this.price == undefined) {
        this.data.alert('Mandatory field missing!', 'danger');
      }

    }

    // buyofferobj['price'] = this.assetprice;
    if (this.ordertype == undefined) {
      type= 'MARKET';
    }
    else {
      var type='MARKET'
     // type = this.ordertype;
    }

    // buyofferobj['msgType'] = 'order_single';
    // var userId = localStorage.getItem('userId');
    // buyofferobj['registID'] = userId;
    //var jsonString = JSON.stringify(buyofferobj);

    this.binanceService.Sellorder(symbol,side,type,timinforce,quantity,price)
      .subscribe(response => {
        if (response !=null) {
        var responsedata=response;
        console.log('---------',responsedata);
        this.data.alert('order placed successfully', 'success');
        }
       


      });

  }
  // connectionstatus() {
  //   var url = 'http://184.72.11.42:7503/BrokerApp/publish/GetSessionStatus/cexio';
  //   if (this.data.courceConn != undefined) {
  //     this.data.courceConn.close();
  //   }
  //   this.data.courceConn = new EventSource(url);

  //   var result: any = new Object();
  //   this.data.courceConn.onmessage = (event: MessageEvent) => {
  //     result = JSON.parse(event.data);
  //     var cunnectdata = result.sessionMaster;
  //     this.exchangename = cunnectdata['sessionName'];
  //     this.msgType = cunnectdata['msgType'];
  //     console.log('/////////////ddd', this.exchangename);
  //   }
  //   // this._CexioService.Getconnectionstatus()
  //   //   .subscribe(data => {
  //   //     console.log('////ddddddd',data.sessionMaster);
  //   //     var logdata=data.sessionMaster;
  //   //     this.exchangename=logdata['sessionName'];
  //   //     this.msgType=logdata['msgType'];
  //   //     //sessionName//
  //   //   })

  // }
  // ngOnDestroy() {
  //   if (this.data.courceConn != undefined) {
  //     this.data.courceConn.close();
  //   }

  // }
}
