import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MarketdataComponent } from './marketdata.component';

const routes: Routes = [{ path: '', component: MarketdataComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketdataRoutingModule { }
