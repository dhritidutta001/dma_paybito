import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarketdataRoutingModule } from './marketdata-routing.module';
import { MarketdataComponent } from './marketdata.component';


@NgModule({
  declarations: [MarketdataComponent],
  imports: [
    CommonModule,
    MarketdataRoutingModule
  ]
})
export class MarketdataModule { }
